package com.vibasafe.viba;

//TODO Valid user 4 digit passcode
//TODO add warning to check if all field are completed


import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;


public class Register extends AppCompatActivity implements View.OnClickListener {

    //UI Widgets
    private Button buttonRegister;
    private Button buttonContact1;
    private Button buttonContact2;
    private EditText editTextUserName;
    private EditText editTextEmail;
    private EditText editTextPassword;
    private EditText editTextPasscode;
    private EditText editTextConfirmPassword;
    private TextView textViewSignIn;
    private ProgressDialog progressDialog;

    //Firebase
    private DatabaseReference database;
    private FirebaseAuth mAuth;

    //Members
    public final int contactResults = 100;
    private User user;
    private Contact contactOne;
    private Contact contactTwo;

    private PhoneNumberUtil phoneUtil;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        //Sets reference to database
        mAuth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance().getReference();

        progressDialog = new ProgressDialog(this);

        //Binds UI Widgets for Program interaction
        buttonRegister = (Button) findViewById(R.id.button_submitSignup);
        buttonContact1 = (Button) findViewById(R.id.button_primarycontact);
        buttonContact2 = (Button) findViewById(R.id.button_secondarycontact);
        editTextUserName = (EditText) findViewById(R.id.edittext_register_username);
        editTextEmail = (EditText) findViewById(R.id.edittext_register_email);
        editTextPassword = (EditText) findViewById(R.id.edittext_register_password);
        editTextConfirmPassword = (EditText) findViewById(R.id.edittext_register_confirmpassword);
        textViewSignIn = (TextView) findViewById(R.id.alreadyRegistered);
        editTextPasscode = (EditText) findViewById(R.id.editText_security_passcode);


        //Set Listeners
        buttonRegister.setOnClickListener(this);
        buttonContact1.setOnClickListener(this);
        buttonContact2.setOnClickListener(this);
        textViewSignIn.setOnClickListener(this);

        phoneUtil = PhoneNumberUtil.getInstance();
    }

    private void registerUser(){

            final String email = editTextEmail.getText().toString();
            final String userName = editTextUserName.getText().toString();
            final String digitalPassword = editTextPasscode.getText().toString();
            String passWord = editTextPassword.getText().toString();

            if (!validateForm()) {
                //Display reason for invalid information
                return;
            }

            progressDialog.setMessage("Registering User...");
            progressDialog.show();
            mAuth.createUserWithEmailAndPassword(email, passWord)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if(task.isSuccessful()){
                                Toast.makeText(Register.this, "User Successfully Registered", Toast.LENGTH_SHORT).show();

                                //Creates User Object and Update Contacts Object
                                user = new User(userName,email,digitalPassword, "", contactOne, contactTwo );

                                if( FirebaseAuth.getInstance().getCurrentUser() != null){
                                    contactOne.setAssociatedUserID(mAuth.getCurrentUser().getUid());
                                    contactTwo.setAssociatedUserID(mAuth.getCurrentUser().getUid());

                                    //Store objects in database
                                    database.child("users").child(mAuth.getCurrentUser().getUid()).setValue(user);
                                    database.child("econtacts").child(contactOne.getPhoneNumber()).setValue(contactOne);
                                    database.child("econtacts").child(contactTwo.getPhoneNumber()).setValue(contactTwo);

                                    //Hide Progress Bar and Redirect User to Homepage
                                    progressDialog.hide();
                                    startActivity(new Intent(Register.this, Homepage.class));
                                } else {
                                    progressDialog.hide();
                                    Toast.makeText(Register.this, "Could not register... please try again", Toast.LENGTH_SHORT).show();
                                }

                            }

                        }
                   });
       }

    @Override
    public void onClick(View v) {
       if(v == buttonRegister){
                registerUser();
        }
        if(v == textViewSignIn){
            //Go back to login page
            startActivity(new Intent(Register.this, Login.class));
            }
        if(v == buttonContact1 || v == buttonContact2){
            getContactInfo();
        }


   }

    public void getContactInfo(){

        Intent contactsIntent = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);

        startActivityForResult(contactsIntent, contactResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Contact ContactInfo;
        // check whether the result is ok
        if (resultCode == RESULT_OK) {
            ContactInfo = parseContactData(data);
                if(contactOne == null)
                    contactOne = ContactInfo;
                else
                    contactTwo = ContactInfo;
            updateUI();
        } else {
            Log.e("MainActivity", "Failed to pick contact");
        }
    }

    private Contact parseContactData(Intent data) {
        Cursor cursor;
        String name;
        String phoneNumber;

        try {

            // getData() method will have the Content Uri of the selected contact
            Uri uri = data.getData();

            //Query the content uri
            cursor = getContentResolver().query(uri, null, null, null, null);
            if(cursor != null && cursor.moveToFirst()) {

                // column index of the phone number
                int phoneIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                int nameIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);

                phoneNumber = cursor.getString(phoneIndex);

                Phonenumber.PhoneNumber numberProto = phoneUtil.parse(phoneNumber, "US");

                phoneNumber= phoneUtil.format(numberProto, PhoneNumberUtil.PhoneNumberFormat.NATIONAL);

                name = cursor.getString(nameIndex);
                cursor.close();

                return new Contact(name, phoneNumber);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        //Failed to get contact
        return null;
    }

    private boolean validateForm() {
        boolean valid = true;

        String userName = editTextUserName.getText().toString();
        String email = editTextEmail.getText().toString();
        String password = editTextPassword.getText().toString();
        String passConfirm = editTextConfirmPassword.getText().toString();


        if (TextUtils.isEmpty(email)) {
            editTextEmail.setError("Required.");
            valid = false;
        } else if( !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches() ){
            editTextEmail.setError("Correct email format required.");

        } else {
            editTextEmail.setError(null);
        }

        if (TextUtils.isEmpty(userName) ) {
            editTextUserName.setError("Required.");
            valid = false;
        }

        if (TextUtils.isEmpty(password) ) {
            editTextPassword.setError("Required.");
            valid = false;
        }
        else if(password.length() < 6)
        {
            editTextPassword.setError("Password must be at least 6 characters long.");
            valid = false;
        }
        else if(TextUtils.isEmpty(passConfirm) ) {
                     editTextConfirmPassword.setError("Required.");
                     valid = false;
        } else if(!passConfirm.equals(password)) {
                     editTextPassword.setError("Passwords do not match.");
                    editTextConfirmPassword.setError("Passwords do not match.");
                    valid = false;
        } else {
            editTextPassword.setError(null);
        }

        return valid;
    }


    private void updateUI(){
        if(contactOne != null) {
            buttonContact1.setText(contactOne.getName());
        }

        if (contactTwo!= null){
            buttonContact2.setText(contactTwo.getName());
        }
    }
}

