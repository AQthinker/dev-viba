package com.vibasafe.viba;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.provider.ContactsContract;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class Homepage extends AppCompatActivity implements View.OnClickListener {

    //Firebase
    private DatabaseReference database;
    private FirebaseAuth mAuth;

    private TextView mECFullNameTextView;
    private TextView mEC2FullNameTextView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homepage);

        //bind Toolbar
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toptoolbar);
        setSupportActionBar(mToolbar);

        //bind views
        mECFullNameTextView = (TextView) findViewById(R.id.EC1FullNameTextView);
        mEC2FullNameTextView = (TextView) findViewById(R.id.EC2FullNameTextView);

        //bind activation button
        findViewById(R.id.activateButton).setOnClickListener(this);
        findViewById(R.id.editEC1Button).setOnClickListener(this);
        findViewById(R.id.editEC2Button).setOnClickListener(this);

        //Get database reference
        mAuth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance().getReference("users").child(mAuth.getCurrentUser().getUid());
        //grab emergency contacts and display
        database.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);

                String contact1Name = user.getContactOne().getName();
                String contact1phone = user.getContactOne().getPhoneNumber();
                mECFullNameTextView.setText(contact1Name + ": " + contact1phone, TextView.BufferType.NORMAL);

                String contact2Name = user.getContactTwo().getName();
                String contact2phone = user.getContactTwo().getPhoneNumber();
                mEC2FullNameTextView.setText(contact2Name + ": " + contact2phone, TextView.BufferType.NORMAL);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

  //toolbar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater mMenuInflater = getMenuInflater();
        mMenuInflater.inflate(R.menu.viba_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.action_setting){
            startActivity(new Intent(Homepage.this, Settings.class));
        }
        if(item.getItemId() == R.id.action_signout){

            startActivity(new Intent(Homepage.this, Login.class));
            finish();
            Toast.makeText(Homepage.this, "You have logged out successfully", Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        startActivity(intent);
    }

    @Override
    public void onClick (View v) {
        if (v.getId() == R.id.activateButton) {
            //switch to listening mode class
            startActivity(new Intent(Homepage.this, ListeningMode.class));
            finish();
        }
        else if(v.getId() == R.id.editEC1Button){
            //switch to settings to edit EC's
            startActivity(new Intent(Homepage.this, Settings.class));
        }
        else if (v.getId() == R.id.editEC2Button){
            //switch to settings to edit EC's
            startActivity(new Intent(Homepage.this, Settings.class));
        }
    }
}