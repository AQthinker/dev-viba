package com.vibasafe.viba;

/**
 * Created by DQ on 5/31/17.
 */

public class Coordinate {

    private double latitude;
    private double longitude;

    public Coordinate(){
        // Default constructor required for calls to DataSnapshot.getValue(com.vibasafe.viba.User.class)
    }

    public  Coordinate (double latitude, double longitude){
        this.setLatitude(latitude);
        this.setLongitude(longitude);
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }
}

