package com.vibasafe.viba;

import android.content.Intent;
import android.provider.ContactsContract;
import android.util.Log;

/**
 * Created by DQ on 5/27/17.
 */

public class Contact {

    public String name;
    public String phoneNumber;
    public String associatedUserID;
    public String currentTokenValue;



    public Contact() {
        // Default constructor required for calls to DataSnapshot.getValue(com.vibasafe.viba.User.class)
    }

    public Contact(String name, String phoneNumber) {
        this.setName(name);
        this.setPhoneNumber(phoneNumber);
        this.setCurrentTokenValue("0000000");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAssociatedUserID() {
        return associatedUserID;
    }

    public void setAssociatedUserID(String associatedUserID) {
        this.associatedUserID = associatedUserID;
    }

    public String getCurrentTokenValue() {
        return currentTokenValue;
    }

    public void setCurrentTokenValue(String currentTokenValue) {
        this.currentTokenValue = currentTokenValue;
    }


}

