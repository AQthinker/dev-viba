package com.vibasafe.viba;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.PhoneNumberUtils;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.vision.text.Text;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import edu.cmu.pocketsphinx.Assets;
import edu.cmu.pocketsphinx.Hypothesis;
import edu.cmu.pocketsphinx.RecognitionListener;
import edu.cmu.pocketsphinx.SpeechRecognizer;
import edu.cmu.pocketsphinx.SpeechRecognizerSetup;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static android.widget.Toast.makeText;

public class  ListeningMode extends AppCompatActivity implements
        View.OnClickListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, RecognitionListener,SensorEventListener {

    private EditText mPasswordEditText;
    private TextView mFalseAlarmTextView;
    private TextView mWeAreListeningTextView;
    private TextView mContactedECs;
    private TextView mCountdownTextView;
    private ImageButton mDeactivateButton;
    private Button mSubmitPasswordButton;

    private AlertDialog alertSettings;
    private AlertDialog alertLogOut;

    private int alertmodeStage = 0;
    private int alertmodeStagePart2 = 0;

    //Google-Play Services
    private static final int PERMISSION_ACCESS_COARSE_LOCATION = 1;
    private GoogleApiClient mGoogleApiClient;

    //Firebase
    private DatabaseReference database;
    private FirebaseAuth mAuth;

    //okhttp
    private OkHttpClient client;
    private Request request;
    private static final String TAG = "ListeningMode";
    private String url = "https://www.google.com/";

    //Trigger Activation/Deactivation
    private boolean countdownActivated = true;
    private CountDownTimer timerLocation;
    private CountDownTimer timerDeactivate;

    public LocationManager locationManager;

    // Hashmaps used for updating database information
    Map <String, Object> updateTrigger = new HashMap<String, Object>();
    Map <String, Object> updateLocation = new HashMap<String, Object>();
    Map <String, Object> updateTextSent = new HashMap<String, Object>();

    private CameraManager objCameraManager;
    private String mCameraId;

    //ContactInfo
    private String contact1number = "";
    private String contact2number = "";

    private String[] userPassword = {"", ""};
    //==========================================================================================================

    /* Named searches allow to quickly reconfigure the decoder */
    private static final String KWS_SEARCH = "wakeup";
    private static final String FORECAST_SEARCH = "forecast";
    private static final String DIGITS_SEARCH = "digits";
    private static final String PHONE_SEARCH = "phones";
    private static final String MENU_SEARCH = "menu";

    /* Keyword we are looking for to activate menu */
    private static final String KEYPHRASE = "oh mighty computer";


    /* Used to handle permission request */
    private static final int PERMISSIONS_REQUEST_RECORD_AUDIO = 1;

    private SpeechRecognizer recognizer;
    private HashMap<String, Integer> captions;


    //============================================================================================================
    private MediaPlayer r;
    private AudioManager mAudioManager;
    //============================================================================================================
    private Sensor appSensor;
    private SensorManager SM;

    private boolean runningMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listening_mode);

        //Get database reference
        mAuth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance().getReference("users").child(mAuth.getCurrentUser().getUid());
        //grab emergency contacts and display
        database.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                userPassword[0] = user.getSecurityCode();
                userPassword[1] = user.getSilentAlarm();
                runningMode = user.isRunningMode();
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        //bind Toolbar
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toptoolbar);
        setSupportActionBar(mToolbar);

        ///Bind Views
        mPasswordEditText = (EditText) findViewById(R.id.passwordEditText);
        mFalseAlarmTextView = (TextView) findViewById(R.id.falseAlarmTextView);
        mWeAreListeningTextView = (TextView) findViewById(R.id.weAreListeningTextView);
        mContactedECs = (TextView) findViewById(R.id.contactedECs);
        mCountdownTextView = (TextView) findViewById(R.id.countdownTextView);
        mDeactivateButton = (ImageButton) findViewById(R.id.deactivateButton);
        mSubmitPasswordButton = (Button) findViewById(R.id.submitPasswordButton);

        //create an listener for onclick buttons
        findViewById(R.id.deactivateButton).setOnClickListener(this);
        findViewById(R.id.submitPasswordButton).setOnClickListener(this);

        //hide user deactivation text box
        mPasswordEditText.setVisibility(View.GONE);
        mFalseAlarmTextView.setVisibility(View.GONE);
        mCountdownTextView.setVisibility(View.GONE);
        mSubmitPasswordButton.setVisibility(View.GONE);

        //change color to green
        String first = "We are ";
        String next = "<font color='#79b94b'>listening</font>";
        mWeAreListeningTextView.setText(Html.fromHtml(first + next));

        //initializing alarm variable
        r = MediaPlayer.create(ListeningMode.this,R.raw.alarm_sound);

        //initializing accelerometer variables
        SM = (SensorManager)getSystemService(SENSOR_SERVICE);
        appSensor = SM.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        SM.registerListener(this, appSensor , SensorManager.SENSOR_DELAY_NORMAL);

        //check if users has connected VIBA to access location
        getUserLocation();
        onStart();
        //start listening for key words
        startListening();

        //start countdown till the next user location update (30 seconds)
        countdownTimer();

        Toast.makeText(ListeningMode.this, "Successfully activated. VIBA is listening", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onClick (View v){
        //Sets reference to database to deactivate user in trigger status
        mAuth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance().getReference();

        //User wants to deactivate VIBA when no trigger word has been said
        if(v.getId()== R.id.deactivateButton && alertmodeStage == 0){
            //Set alert mode on for EC by updating the user trigger status in the database
            updateTrigger.put("triggerStatus", "0");
            database.child("users").child(mAuth.getCurrentUser().getUid()).updateChildren(updateTrigger);

            //The ALARM is turned off
            makeSomeNoise(false);

            startActivity(new Intent(ListeningMode.this, Homepage.class));
            android.os.Process.killProcess(android.os.Process.myPid());
            finish();
            Toast.makeText(ListeningMode.this, "You successfully deactivated VIBA", Toast.LENGTH_SHORT).show();
        }
        else if(v.getId()== R.id.submitPasswordButton){
            //lets add a silent alarm <TODO>
            if(userPassword[0].equals(mPasswordEditText.getText().toString())){
                //Set alert mode on for EC by updating the user trigger status in the database
                updateTrigger.put("triggerStatus", "0");
                database.child("users").child(mAuth.getCurrentUser().getUid()).updateChildren(updateTrigger);

                //user entered password successfully
                successfulMessage();
                timerForDialogDisplay();
                onPauseTimer();
                //ALARM is turned off
                makeSomeNoise(false);
            }
            //SILENT ALARM
            else if(userPassword[1].equals(mPasswordEditText.getText().toString())){
                //send EC's but still show successful
                contactECs();
                //DO NOT change trigger status, they are still in trouble
                //user entered password successfully
                successfulMessage();
                timerForDialogDisplay();
                onPauseTimer();
                //ALARM is turned off
                makeSomeNoise(false);
            }
            else{
                Toast.makeText(ListeningMode.this, "Wrong Password. Please try again.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void getUserLocation(){
        //ask for permission to access location if its not set up yet
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION},
                    PERMISSION_ACCESS_COARSE_LOCATION);
        }
        //request to build GoogleApiClient
        mGoogleApiClient = new GoogleApiClient.Builder(this, this, this).addApi(LocationServices.API).build();
    }

    protected void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }
    @Override
    public void onConnected(Bundle connectionHint) {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {

            LocationAvailability locationAvailability = LocationServices.FusedLocationApi.getLocationAvailability(mGoogleApiClient);
            Location lastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

            if (lastLocation != null) {
                double lat = lastLocation.getLatitude();
                double lon = lastLocation.getLongitude();

                Coordinate current = new Coordinate(lat, lon);

                //Store location (lat and long) in database
                database = FirebaseDatabase.getInstance().getReference();
                updateLocation.put("location", current);
                database.child("users").child(mAuth.getCurrentUser().getUid()).updateChildren(updateLocation);

                //Toast.makeText(ListeningMode.this, "Successfully located lat: " + lat + "and long: " + lon, Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void countdownTimer(){
        if(countdownActivated) {
            timerLocation = new CountDownTimer(450000, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    //Toast.makeText(ListeningMode.this, "Seconds Remaining: " + (millisUntilFinished / 1000), Toast.LENGTH_SHORT).show();
                    if (!countdownActivated) {
                        timerLocation.cancel();
                    }
                }
                @Override
                public void onFinish() {
                    //relocate user and repeat steps
                    getUserLocation();
                    onStart();
                    countdownTimer();
                }
            };
            timerLocation.start();
        }
        if(!countdownActivated) {
            timerLocation.cancel();
        }
    }

    public void makeSomeNoise(boolean panic)
    {
        if(panic == true)
        {
            try{
                r.setLooping(true);
                r.start();
            }
            catch (Exception e) {
                e.printStackTrace();
            }

        }
        else
            r.stop();

    }

    //Alert MODE
    public void alertMode(){
        //alert user whats happening
        String first = "VIBA Detects Trouble: ";
        String next = "<font color='#ff1a1a'>False Alarm?</font>";
        mWeAreListeningTextView.setText(Html.fromHtml(first + next));

        alertmodeStage++;
        //set circle to red
        mDeactivateButton.setBackgroundResource(R.drawable.alert_round_button);
        
        if(alertmodeStage == 1) {

            //Sets reference to database
            mAuth = FirebaseAuth.getInstance();
            database = FirebaseDatabase.getInstance().getReference();

            //Set alert mode on for EC by updating the user trigger status in the database
            alertDatabase();
            updateTrigger.put("triggerStatus", "1");
            database.child("users").child(mAuth.getCurrentUser().getUid()).updateChildren(updateTrigger);

            //show password text box for user input
            mPasswordEditText.setVisibility(View.VISIBLE);
            mFalseAlarmTextView.setVisibility(View.VISIBLE);
            mCountdownTextView.setVisibility(View.VISIBLE);
            mSubmitPasswordButton.setVisibility(View.VISIBLE);

            //set and display timer for 30 seconds for user to type in password
            timerDeactivate = new CountDownTimer(45000, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    String secondsRemaining = String.valueOf(millisUntilFinished / 1000);
                    mCountdownTextView.setText(secondsRemaining);
                }

                @Override
                public void onFinish() {

                    //failed to provide password
                    Toast.makeText(ListeningMode.this, "Password Failed", Toast.LENGTH_SHORT).show();
                    alertmodeStagePart2++;
                    //only text them the first time
                    if (alertmodeStagePart2 == 1) {
                        //message EC's
                        contactECs();
                        //Toast.makeText(ListeningMode.this, "Texted EC's", Toast.LENGTH_SHORT).show();

                        mCountdownTextView.setTextColor(getResources().getColor(R.color.alertRED));
                        ((TextView) findViewById(R.id.caption_text)).setText("");
                        String first = "Current Status: ";
                        String next = "<font color='#ff1a1a'>Alert Mode</font>";
                        mWeAreListeningTextView.setText(Html.fromHtml(first + next));
                        mContactedECs.setText("Viba successfully texted your Emergency Contacts");

                        //start timer again for 2nd stage of alert mode
                        timerDeactivate.start();

                    } else {
                        startActivity(new Intent(ListeningMode.this, Homepage.class));
                        makeSomeNoise(false);
                        makeCall();
                    }
                }//end of onFinish
            };
            timerDeactivate.start();
            //sound the alarm/flash the light
            makeSomeNoise(true);
        }
    }

    public void onPauseTimer() {
        timerDeactivate.cancel();
    }

    //Text EC's after FIRST timer in alert mode has passed
    public void contactECs(){

        mAuth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance().getReference("users").child(mAuth.getCurrentUser().getUid());
        final DatabaseReference triggerDatabase =FirebaseDatabase.getInstance().getReference("triggeredUsers").child(mAuth.getCurrentUser().getUid());
        database.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);

                //Add code for sms//test only needs to be moved to correct location
                contact1number = user.getContactOne().getPhoneNumber();
                contact2number = user.getContactTwo().getPhoneNumber();
                contact1number = contact1number.replaceAll(" ", "%20");
                contact2number = contact2number.replaceAll(" ", "%20");


                try {
                    connectToServer(contact1number);
                    connectToServer(contact2number);

                    // If success update the database the text message has been sent
                    updateTextSent.put("textSent", "1");
                    triggerDatabase.updateChildren(updateTextSent);
                } catch (IOException e){
                    e.printStackTrace();
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });
    }

    public void timerForDialogDisplay(){
        final CountDownTimer timer = new CountDownTimer(4000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                //display the user a successful password input message
            }
            @Override
            public void onFinish() {
                startActivity(new Intent(ListeningMode.this, Homepage.class));
                android.os.Process.killProcess(android.os.Process.myPid());
            }
        }; timer.start();
    }

    @Override
    public void onConnectionSuspended(int i) {
        Toast.makeText(ListeningMode.this, "Connection was suspended", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(ListeningMode.this, "Connection failed", Toast.LENGTH_SHORT).show();
    }

    //toolbar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater mMenuInflater = getMenuInflater();
        mMenuInflater.inflate(R.menu.viba_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.action_setting){
            //Toast.makeText(ListeningMode.this, "Setting was selected", Toast.LENGTH_SHORT).show();
            //ask user if they are sure they want to exit listening mode
            alertSettings = new AlertDialog.Builder(ListeningMode.this)
                    .setTitle("Are you sure you want to deactivate VIBA?")
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            startActivity(new Intent(ListeningMode.this, Settings.class));
                        }
                    })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            //cancel AlertDialog
                            dialog.dismiss();
                            alertSettings.cancel();
                        }
                    })
                    .create();
            alertSettings.show();
        }
        if(item.getItemId() == R.id.action_signout){

            //Toast.makeText(ListeningMode.this, "You have logged out", Toast.LENGTH_SHORT).show();

            //startActivity(new Intent(ListeningMode.this, Login.class));
            //android.os.Process.killProcess(android.os.Process.myPid());
            //finish();


            alertLogOut = new AlertDialog.Builder(ListeningMode.this)
                    .setTitle("Are you sure you want to deactivate VIBA?")
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {

                            startActivity(new Intent(ListeningMode.this, Login.class));
                            android.os.Process.killProcess(android.os.Process.myPid());
                            finish();
                            //Toast.makeText(ListeningMode.this, "You have logged out successfully", Toast.LENGTH_SHORT).show();
                        }
                    })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            //cancel AlertDialog
                            alertLogOut.cancel();
                        }
                    })
                    .create();
            alertLogOut.show();
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        startActivity(intent);
    }


    //==============================================================================================

    public void startListening()
    {

        //((TextView) findViewById(R.id.caption_text)).setText("Preparing the recognizer");

        try
        {
            Assets assets = new Assets(this);
            File assetDir = assets.syncAssets();
            setupRecognizer(assetDir);
        }
        catch (IOException e)
        {
            //
            Toast.makeText(ListeningMode.this, "An Error has occured", Toast.LENGTH_SHORT).show();
        }

       /* ((TextView) findViewById(R.id.caption_text)).setText("Say the following words to activate me:" +
                " help, stop, creep, danger, go away, get off me");*/

        reset();
    }

    @Override
    public void onPartialResult(Hypothesis hypothesis)
    {
    }

    @Override
    public void onResult(Hypothesis hypothesis)
    {
        ((TextView) findViewById(R.id.result_text)).setText("");

        if (hypothesis != null)
        {

            String text = hypothesis.getHypstr();
            //makeText(getApplicationContext(), "I said: "+text, Toast.LENGTH_SHORT).show();
            alertMode();

        }
    }

    @Override
    public void onError(Exception e) {

    }

    @Override
    public void onTimeout() {

    }

    @Override
    public void onBeginningOfSpeech()
    {
    }

    @Override
    public void onEndOfSpeech()
    {
        reset();
    }

    private void setupRecognizer(File assetsDir) throws IOException {
        File modelsDir = new File(assetsDir, "models");

        recognizer = SpeechRecognizerSetup.defaultSetup().setAcousticModel(new File(assetsDir, "en-us-ptm"))
                .setDictionary(new File(assetsDir, "cmudict-en-us.dict"))
                .setRawLogDir(assetsDir).setKeywordThreshold(1e-20f)
                .getRecognizer();

        recognizer.addListener(this);

        File digitsGrammar = new File(assetsDir, "digits.gram");
        recognizer.addKeywordSearch(DIGITS_SEARCH, digitsGrammar);
    }

    private void reset()
    {
        recognizer.stop();
        recognizer.startListening(DIGITS_SEARCH);
    }

    //==============================================================================================

    private void connectToServer( String contactNumber ) throws IOException{

        OkHttpClient client = new OkHttpClient();

        String url = "https://www.vibasafe.com/api/sendsms/" + contactNumber;
        Request request = new Request.Builder()
                .url(url)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                call.cancel();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                final String myResponse = response.body().string();
                Log.i("OKHTTP", myResponse);

            }
        });
    }

    public void successfulMessage(){
        //user entered password successfully
        Dialog dialog = new Dialog(ListeningMode.this);
        dialog.setContentView(R.layout.activity_deactivation_successful);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.getWindow().setAttributes(lp);
        //now that the dialog is set up, it's time to show it
        dialog.show();
    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        if(runningMode == false)
        {
            if(event.values[0] > 8.0)
            {
                //Toast.makeText(ListeningMode.this, "x axis " + event.values[0], Toast.LENGTH_SHORT).show();
                SM.unregisterListener(this);
                alertMode();
            }
            if(event.values[1] > 14.0)
            {
                //Toast.makeText(ListeningMode.this, "y axis" + event.values[1], Toast.LENGTH_SHORT).show();
                SM.unregisterListener(this);
                alertMode();
            }

            if(event.values[2] > 17.0)
            {
                //Toast.makeText(ListeningMode.this, "z axis " + event.values[2], Toast.LENGTH_SHORT).show();
                SM.unregisterListener(this);
                alertMode();
            }

        }
        
    }
    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        //function does nothing but is required by the sensor event listener
    }

    private void makeCall() {

        try {
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:911"));
            startActivity(callIntent);
        } catch (SecurityException err) {
            //Handle error
        }
    }

    private void alertDatabase(){
        //Sets reference to database
        mAuth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance().getReference();

        // Sets reference for timestamp
        long dtMili = System.currentTimeMillis();

        //Write User status to Active Database
        database.child("triggeredUsers").child(mAuth.getCurrentUser().getUid()).child("triggerStatus").setValue("1");
        database.child("triggeredUsers").child(mAuth.getCurrentUser().getUid()).child("textSent").setValue("0");
        database.child("triggeredUsers").child(mAuth.getCurrentUser().getUid()).child("timeStamp").setValue(dtMili);

    }

}