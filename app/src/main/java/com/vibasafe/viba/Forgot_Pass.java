package com.vibasafe.viba;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class Forgot_Pass extends AppCompatActivity implements View.OnClickListener
{
    //UI Widgets
    private EditText editTextEmail;

    //Firebase
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                setContentView(R.layout.activity_forgot__pass);

                mAuth = FirebaseAuth.getInstance();

                editTextEmail = (EditText) findViewById(R.id.userEmailEditText);
                findViewById(R.id.button_recoverPassword).setOnClickListener(this);
                findViewById(R.id.returnToLoginTextView).setOnClickListener(this);

               // buttonRecover.setOnClickListener(this);
            }

            private void recoverPass()
            {
                final String email = editTextEmail.getText().toString();

                if (!validateForm())
                {
                    //Display reason for invalid information
                    return;
                }

                mAuth.sendPasswordResetEmail(email)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                    public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    Toast.makeText(Forgot_Pass.this, "An email has been sent to reset your password", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
            }

            @Override
            public void onClick(View v)
            {
                if(v.getId()== R.id.button_recoverPassword)
                {
                    recoverPass();
                }
                else if(v.getId() == R.id.returnToLoginTextView)
                {
                    startActivity(new Intent(Forgot_Pass.this, Login.class));
                }
            }

            private boolean validateForm()
            {
                boolean valid = true;

                String email = editTextEmail.getText().toString();
                if (TextUtils.isEmpty(email)) {
                    editTextEmail.setError("Required.");
                    valid = false;
                }
                else if( !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches() )
               {
                    editTextEmail.setError("Correct email format required.");
                }
                else {
                    editTextEmail.setError(null);
                }

                return valid;
            }

        }