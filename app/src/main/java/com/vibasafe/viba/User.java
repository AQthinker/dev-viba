package com.vibasafe.viba;

/**
 * Created by DQ on 5/27/17.
 */

public class User {

    private String userEmail;
    private String userName;
    private String securityCode;
    private String silentAlarm;
    private Contact contactOne;
    private Contact contactTwo;
    private String triggerStatus;
    private Coordinate location;
    private boolean runningMode;


    /***
     * Constructors
     *
     */


    public User() {
        // Default constructor required for calls to DataSnapshot.getValue(com.vibasafe.viba.User.class)
    }

    public User(String userName, String userEmail, String securityCode, String silentAlarm, Contact contactOne , Contact contactTwo) {
        this.setUserName(userName);
        this.setUserEmail(userEmail);
        this.setSecurityCode(securityCode);
        this.setSilentAlarm(silentAlarm);
        this.setContactOne(contactOne);
        this.setContactTwo(contactTwo);
        this.setLocation(0,0);
        this.setRunningMode(false);
    }

    /***
     * Getters and Setters
     *
     */

    public Coordinate getLocation() {
        return location;
    }

    public void setLocation( double latitude, double longitude) {
        this.location = new Coordinate(latitude,longitude);
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    private void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getSecurityCode() {
        return securityCode;
    }

    public String getSilentAlarm() {
        return silentAlarm;
    }

    public boolean setSecurityCode(String securityCode) {
        //set check for valid security code
        //Must be four digits
        //Can't be a series of all repeating numbers
        //Can't be a series of numbers all in order
        this.securityCode = securityCode;

        return true;
    }

    public boolean setSilentAlarm(String silentAlarm) {
        //set check for valid security code
        //Must be four digits
        //Can't be a series of all repeating numbers
        //Can't be a series of numbers all in order
        this.silentAlarm = silentAlarm;

        return true;
    }

    public Contact getContactOne() {
        return contactOne;
    }

    public boolean setContactOne(Contact contactOne) {
        //set checks for valid contact number
        //Cannot be the same contact as contact two
        this.contactOne = contactOne;

        return true;
    }

    public Contact getContactTwo() {
        return contactTwo;
    }

    public void setContactTwo(Contact contactTwo) {
        //set checks for valid contact number
        //Cannot be the same contact as contact one
        this.contactTwo = contactTwo;
    }


    public String getTriggerStatus() {
        return triggerStatus;
    }

    public void setTriggerStatus(String triggerStatus) {
        this.triggerStatus = triggerStatus;
    }


    public boolean isRunningMode() {
        return runningMode;
    }

    public void setRunningMode(boolean runningMode) {
        this.runningMode = runningMode;
    }

}
