package com.vibasafe.viba;

//TODO Add forgot password functionality

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthInvalidUserException;
import com.google.firebase.auth.FirebaseUser;

public class Login extends AppCompatActivity implements View.OnClickListener{

    private final String TAG = "USER_LOGIN";

    EditText mEmailView;
    EditText mPasswordView;
    ProgressDialog progressDialog;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        progressDialog = new ProgressDialog(this);

        //Bind Views
        mEmailView = (EditText) findViewById(R.id.edittext_email);
        mPasswordView = (EditText) findViewById(R.id.edittext_password);

        //OnClicklistner
        findViewById(R.id.button_login).setOnClickListener(this);
        findViewById(R.id.button_forgotlogin).setOnClickListener(this);
        findViewById(R.id.button_signup).setOnClickListener(this);

        //initialize AuthListener
        mAuth = FirebaseAuth.getInstance();

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // com.vibasafe.viba.User is signed in
                    Log.d(TAG, "Signed in: " + user.getUid());
                } else {
                    // com.vibasafe.viba.User is signed out
                    Log.d(TAG, "Currently signed out");
                }

            }
        };
    }

    private void signIn(){
        final String email = mEmailView.getText().toString();
        final String password = mPasswordView.getText().toString();

        //Check if Email and Password are in the correct format
        Log.d(TAG, "signIn:" + email);
        if (!validateForm()) {
            return;
        }

        progressDialog.setMessage("Signing in...");
        progressDialog.show();

        //Call mAuth to attempt sign in
        mAuth.signInWithEmailAndPassword(email,password)
                .addOnCompleteListener(this,
                        new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {
                                    progressDialog.hide();

                                    Toast.makeText(Login.this, "Signed in", Toast.LENGTH_SHORT)
                                            .show();
                                    startActivity(new Intent(Login.this, Homepage.class));
                                    finish();

                                }
                            }
                        }
                )
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        progressDialog.hide();
                        if (e instanceof FirebaseAuthInvalidCredentialsException) {
                            Toast.makeText(Login.this, "Incorrect Password", Toast.LENGTH_LONG)
                                    .show();
                        }
                        else if (e instanceof FirebaseAuthInvalidUserException) {
                            Toast.makeText(Login.this, "No Account with Email", Toast.LENGTH_LONG)
                                    .show();
                        }
                        else {
                            Toast.makeText(Login.this, "Sign in failed", Toast.LENGTH_SHORT)
                                    .show();
                        }
                    }
                });

    }

    private boolean validateForm() {
        boolean valid = true;

        String email = mEmailView.getText().toString();
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError("Required.");
            valid = false;
        } else {
            mEmailView.setError(null);
        }

        String password = mPasswordView.getText().toString();
        if (TextUtils.isEmpty(password)) {
            mPasswordView.setError("Required.");
            valid = false;
        } else {
            mPasswordView.setError(null);
        }

        return valid;
    }

    @Override
    public void onClick (View v){
        if(v.getId()== R.id.button_login){
            signIn();
        }
        else if(v.getId() == R.id.button_signup){
            registerUser();
        }
        else if(v.getId() ==  R.id.button_forgotlogin)
        {
            startActivity(new Intent(Login.this, Forgot_Pass.class));
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    private void registerUser(){

        startActivity(new Intent(Login.this, Register.class));
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        startActivity(intent);
    }

}
