package com.vibasafe.viba;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;

import java.util.HashMap;
import java.util.Map;

public class Settings extends AppCompatActivity implements View.OnClickListener {

    private EditText mEmailEditText;
    private EditText mPasswordEditText;
    private EditText mSilentEditText;
    private Button mPrimary1contactButton;
    private Button mPrimary2contactButton;
    private Switch mToggleSwitch;
    private boolean runningMode = true;

    //Firebase
    private DatabaseReference database;
    private FirebaseAuth mAuth;

    //Members
    public final int contactResults = 100;
    private User user;
    private Contact contactOne;
    private Contact contactTwo;
    private int contactClicked;

    // Hashmaps used for updating database information
    Map<String, Object> updateEmail = new HashMap<String, Object>();
    Map <String, Object> updatePassword = new HashMap<String, Object>();
    Map <String, Object> updateSilentAlarm = new HashMap<String, Object>();
    Map<String, Object> updateEC1 = new HashMap<String, Object>();
    Map <String, Object> updateEC2 = new HashMap<String, Object>();
    Map <String, Object> updateisRunnning = new HashMap<String, Object>();

    private AlertDialog alertSettings;
    private boolean backButtonEvent = true;
    private String status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        //bind TextViews to populate title from database
        mEmailEditText = (EditText) findViewById(R.id.emailEditText);
        mPasswordEditText = (EditText) findViewById(R.id.passwordEditText);
        mSilentEditText = (EditText) findViewById(R.id.silentEditText);
        mPrimary1contactButton = (Button) findViewById(R.id.primary1contactButton);
        mPrimary2contactButton = (Button) findViewById(R.id.primary2contactButton);
        mToggleSwitch = (Switch) findViewById(R.id.toggleSwitch);
        //Get database reference
        mAuth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance().getReference("users").child(mAuth.getCurrentUser().getUid());
        //grab emergency contacts and display
        database.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);

                String userEmail = user.getUserEmail();
                mEmailEditText.setText(userEmail, TextView.BufferType.NORMAL);

                String userSC = user.getSecurityCode();
                mPasswordEditText.setText(userSC, TextView.BufferType.NORMAL);

                String userSA = user.getSilentAlarm();
                mSilentEditText.setText(userSA, TextView.BufferType.NORMAL);

                String userEC1Name = user.getContactOne().getName();
                mPrimary1contactButton.setText(userEC1Name, TextView.BufferType.NORMAL);
                contactOne = user.getContactOne();

                String userEC2Name = user.getContactTwo().getName();
                mPrimary2contactButton.setText(userEC2Name, TextView.BufferType.NORMAL);
                contactTwo = user.getContactTwo();

                runningMode = user.isRunningMode();
                mToggleSwitch.setChecked(runningMode);
                if(runningMode == true)
                    status = "ON";
                else
                    status = "OFF";
                mToggleSwitch.setText("Running Mode "+status);



            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
       // mToggleSwitch.setChecked(true);

        //create an listener for onclick buttons
        findViewById(R.id.primary1contactButton).setOnClickListener(this);
        findViewById(R.id.primary2contactButton).setOnClickListener(this);
        findViewById(R.id.saveSettingsButton).setOnClickListener(this);

        // attach an OnClickListener
        mToggleSwitch.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton toggleButton, boolean isChecked) {
                if(isChecked){
                    mToggleSwitch.setText("Running Mode ON");
                    runningMode = true;
                }else{
                    mToggleSwitch.setText("Running Mode OFF");
                    runningMode = false;
                }
            }
        });

    }

    @Override
    public void onClick(View v) {
        if(v.getId()== R.id.saveSettingsButton){
            //SAVE ALL VARIABLES TO DATABASE
            saveUserInfo();
        }
        else if(v.getId() == R.id.primary1contactButton){
            contactClicked = 1;
            getContactInfo();
        }
        else if(v.getId() == R.id.primary2contactButton){
            contactClicked = 2;
            getContactInfo();
        }

    }

    public void getContactInfo(){

        Intent contactsIntent = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);

        startActivityForResult(contactsIntent, contactResults);
    }

    public void saveUserInfo(){

        if (!validateForm()) {
            //Display reason for invalid information
            return;
        }
        else {

            updateEmail.put("userEmail", mEmailEditText.getText().toString());
            database.updateChildren(updateEmail);

            updatePassword.put("securityCode", mPasswordEditText.getText().toString());
            database.updateChildren(updatePassword);

            updateSilentAlarm.put("silentAlarm", mSilentEditText.getText().toString());
            database.updateChildren(updateSilentAlarm);

            updateEC1.put("contactOne", contactOne);
            database.updateChildren(updateEC1);

            updateEC2.put("contactTwo", contactTwo);
            database.updateChildren(updateEC2);

            updateisRunnning.put("runningMode", runningMode);
            database.updateChildren(updateisRunnning);

            startActivity(new Intent(Settings.this, Homepage.class));
        }
    }
    public boolean validateForm(){
        boolean valid = true;

        String email = mEmailEditText.getText().toString();
        if (TextUtils.isEmpty(email)) {
            mEmailEditText.setError("Required.");
            valid = false;
        } else if( !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches() ){
            mEmailEditText.setError("Correct email format required.");
            valid = false;
        }
        if (contactOne == null){
            mPrimary1contactButton.setError("Please select an Emergency Contact");
            valid = false;
        }
        if(contactTwo == null){
            mPrimary2contactButton.setError("Please select an Emergency Contact");
            valid = false;
        }
        String password = mPasswordEditText.getText().toString();
        if (TextUtils.isEmpty(password) ) {
            mPasswordEditText.setError("Required.");
            valid = false;
        }
        return valid;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Contact ContactInfo;
        // check whether the result is ok
        if (resultCode == RESULT_OK) {
            ContactInfo = parseContactData(data);
            if(contactClicked == 1)
                contactOne = ContactInfo;
            else
                contactTwo = ContactInfo;
            updateUI();
        } else {
            Toast.makeText(Settings.this, "Failed to pickup contact", Toast.LENGTH_LONG).show();
        }
    }
    private Contact parseContactData(Intent data) {
        Cursor cursor;
        String name;
        String phoneNumber;

        try {

            // getData() method will have the Content Uri of the selected contact
            Uri uri = data.getData();

            //Query the content uri
            cursor = getContentResolver().query(uri, null, null, null, null);
            cursor.moveToFirst();

            // column index of the phone number
            int  phoneIndex =cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
            int  nameIndex =cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);

            phoneNumber = cursor.getString(phoneIndex);
            name = cursor.getString(nameIndex);
            cursor.close();

            return new Contact(name, phoneNumber);

        } catch (Exception e) {
            e.printStackTrace();
        }

        //Failed to get contact
        return null;
    }

    private void updateUI(){
        if(contactOne != null) {
            mPrimary1contactButton.setText(contactOne.getName().toString());
        }

        if (contactTwo!= null){
            mPrimary2contactButton.setText(contactTwo.getName().toString());
        }
    }

    //ASK USER IF THEY ARE SURE THEY WANT TO EXIT W/ OUT SAVING
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            alertSettings = new AlertDialog.Builder(Settings.this)
                    .setTitle("Are you sure you want to leave before saving your changes?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            finish();

                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                        alertSettings.cancel();
                        }
                    })
                    .create();
            alertSettings.show();
        }
        if(!backButtonEvent){
            return false;
        }
            return super.onKeyDown(keyCode, event);
    }
}