package com.vibasafe.viba;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.Toast;

public class DeactivationSuccessful extends AppCompatActivity {

    private CountDownTimer timerDeactivate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deactivation_successful);

        Toast.makeText(DeactivationSuccessful.this, "Your PASSWORD was authorized", Toast.LENGTH_SHORT).show();
    }
}
