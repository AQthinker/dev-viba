/**
 * Automatically generated file. DO NOT MODIFY
 */
package edu.cmu.pocketsphinx.demo;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "edu.cmu.pocketsphinx.demo";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 2;
  public static final String VERSION_NAME = "1.1";
}
